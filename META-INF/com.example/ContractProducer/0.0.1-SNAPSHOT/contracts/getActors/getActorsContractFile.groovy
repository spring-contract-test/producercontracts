package contracts.getActors

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description("should return the list of all the actors in world")
    request {
        url "/actors"
        method GET()
    }
    response {
        status 201
        body(["Git","Martin"])
    }
}